//
//  TTCollectionView.h
//  collection-test
//
//  Created by Adison Wu on 2016/8/10.
//  Copyright © 2016年 adison. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "Person.h"

@interface TTCollectionView : NSCollectionView

@end
