//
//  Person.h
//  collection-test
//
//  Created by Adison Wu on 2016/8/23.
//  Copyright © 2016年 adison. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Person : NSObject

@property (nonatomic) NSString* name;

@property (nonatomic) NSString* date;
@end
