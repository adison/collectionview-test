//
//  ViewController.m
//  collection-test
//
//  Created by Adison Wu on 2016/8/8.
//  Copyright © 2016年 adison. All rights reserved.
//

#import "ViewController.h"
#import "CollectionItem.h"
#import "TTCollectionView.h"

@interface ViewController() {
    NSMutableArray *pplist;
    CollectionItem* itemPrototype;
}
@property (weak) IBOutlet TTCollectionView *collectView;
@end

@implementation ViewController
@synthesize listController;

static bool cc = false;
- (IBAction)row1:(id)sender {
    
    if(cc) {
        for(Person* pp in pplist) {
            pp.name = @"323";
            pp.date = @"456";
        }
    }
    else {
        for(Person* pp in pplist) {
            pp.name = @"cccc";
            pp.date = @"ddd";
        }
    }
    cc = !cc;
}

- (IBAction)row2:(id)sender {
    // change layout
    [_collectView updateConstraints];
    
    // change item-prototype
    itemPrototype = [[CollectionItem alloc] initWithNibName:@"StraightCollectionItem" bundle:[NSBundle mainBundle]];
    itemPrototype.straightStyle = true;
    [_collectView setItemPrototype:itemPrototype];
    
    // reload data & layout
    CGSize itemSize = NSMakeSize(300, 100);
    [_collectView setMaxItemSize:itemSize];
    [_collectView setMinItemSize:itemSize];

}


-(void)viewWillAppear {
    // strat collection view

    // test 1: 使用 array controlller
//    listController.content = pplist;
//    _collectView.content = listController.arrangedObjects;

    // test 2: 直接提供陣列
    _collectView.content = pplist;
    
    [super viewWillAppear];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.wantsLayer = YES;
    
    // init array controller
    listController = [NSArrayController new];
    pplist = [NSMutableArray new];

    
    for (NSInteger i= 0, ii= 30; i< ii; i++) {
        Person* pp = [Person new];
        pp.name = [NSString stringWithFormat:@"%015ld", i];
        pp.date = [NSString stringWithFormat:@"%07ld", i*i];
        [pplist addObject:pp];
    }
    // 這兩句意思相同
    //    [listController addObject:pp];
    //    [listController setContent:pplist];
    
    
    CGFloat width = [[_collectView enclosingScrollView] bounds].size.width;
    CGSize itemSize = NSMakeSize(width, 100);
    [_collectView setMaxItemSize:itemSize];
    [_collectView setMinItemSize:itemSize];
    
    itemPrototype = [[CollectionItem alloc] initWithNibName:@"CollectionItem" bundle:[NSBundle mainBundle]];
    itemPrototype.straightStyle = true;
    
    [_collectView setItemPrototype:itemPrototype];

    
}

- (void)setRepresentedObject:(id)representedObject {
    [super setRepresentedObject:representedObject];
}

@end
