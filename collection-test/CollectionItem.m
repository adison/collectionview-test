//
//  CollectionItem.m
//  collection-test
//
//  Created by Adison Wu on 2016/8/8.
//  Copyright © 2016年 adison. All rights reserved.
//

#import "CollectionItem.h"
#import <QuartzCore/QuartzCore.h>

@interface CollectionItem ()

@end

@implementation CollectionItem
@synthesize header, desc, straightStyle;

- (instancetype)init {
    self = [super init];
    if (self) {
    }
    return self;
}

-(void)viewWillAppear {
    [super viewWillAppear];
    [self setRepresentedObject:self.representedObject];
}


-(void)setRepresentedObject:(id)representedObject{
    [super setRepresentedObject:representedObject];
    if (representedObject ==nil)
        return;

    [representedObject addObserver:self forKeyPath:@"name" options:NSKeyValueObservingOptionNew context:@"1"];
    [representedObject addObserver:self forKeyPath:@"date" options:NSKeyValueObservingOptionNew context:@"2"];
    
    header.stringValue = [(Person*)representedObject valueForKey:@"name"];
    desc.stringValue = [(Person*)representedObject valueForKey:@"date"];
}


-(void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSString *,id> *)change context:(void *)context {
    if ([keyPath isEqualToString:@"name"]) {
        header.stringValue = [self.representedObject valueForKey:@"name"];
    }
    else {
        desc.stringValue = [self.representedObject valueForKey:@"date"];
        
    }
}


-(id)copy {
    if (straightStyle) {
        id copy = [[CollectionItem alloc]initWithNibName:@"StraightCollectionItem" bundle:[NSBundle mainBundle]];
        return copy;
    }
    id copy = [[CollectionItem alloc]initWithNibName:@"CollectionItem" bundle:[NSBundle mainBundle]];
    
    return copy;
}
@end
